﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Menu : Form
    {       
        public Menu()
        {
            InitializeComponent();
        }       

        private void Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario();
            user.MdiParent = this;
            user.Show();
        }

        private void modelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MantoModelo user = new MantoModelo();
            user.MdiParent = this;
            user.Show();
        }

        private void busquedaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BusquedaModelo  user = new BusquedaModelo();
            user.MdiParent = this;
            user.Show();
        }

        private void form1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 user = new Form1();
            user.MdiParent = this;
            user.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MantoUsuario : Form
    {

        private string user { get; set; }

        public MantoUsuario()
        {
            InitializeComponent();
            user = "";
        }

        public MantoUsuario(string usuario)
        {            
            InitializeComponent();
            this.user = usuario;
        }

        private void MantoUsuario_Load(object sender, EventArgs e)
        {
            if(user != "")
            {
                DSAutos.ususarioDataTable tabla = new DSAutos.ususarioDataTable();
                DSAutosTableAdapters.ususarioTableAdapter tableAdapter = new DSAutosTableAdapters.ususarioTableAdapter();
                int error = tableAdapter.FillByUsuario(tabla, user);
                if (tabla.Rows.Count > 0)
                {
                    TBUsuario.Text = tabla.Rows[0]["usuario"].ToString();
                    TBContraseña.Text = tabla.Rows[0]["constraseña"].ToString();
                }
                else
                {
                    MessageBox.Show("No se encontró la información del usuario seleccionado");
                    this.Close();
                }
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (TBUsuario.Text.Trim() == "" || TBContraseña.Text.Trim() == "")
                {
                    MessageBox.Show("Debe ingresar usuario y/o contraseña");
                }
                else
                {
                    DSAutosTableAdapters.ususarioTableAdapter tableAdapter = new DSAutosTableAdapters.ususarioTableAdapter();
                    int error = 0;

                    if (user == "")
                    {
                        error = tableAdapter.Insert(TBUsuario.Text.Trim(), TBContraseña.Text.Trim());
                    }
                    else
                    {
                        error = tableAdapter.Update(TBUsuario.Text.Trim(), TBContraseña.Text.Trim(), user);
                    }
                                        
                    if (error == 1)
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Ha ocurrido un error al guardar el usuario");
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
                
    }
}

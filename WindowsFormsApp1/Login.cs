﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            autenticar(TBUsuario.Text.Trim(), TBContraseña.Text.Trim());
        }


        private void autenticar(string usuario, string contraseña)
        {
            if(usuario =="" || contraseña =="")
            {
                MessageBox.Show("Debe ingresar usuario y/o contraseña");
            }
            else
            {            
                DSAutos.ususarioDataTable tabla = new DSAutos.ususarioDataTable();

                DSAutosTableAdapters.ususarioTableAdapter  tableAdapter = new DSAutosTableAdapters.ususarioTableAdapter();

                int error = tableAdapter.FillByUsuarioContraseña(tabla,usuario, contraseña);

                if (tabla.Rows.Count > 0)
                {
                    sesion.usuario = usuario;
                    Menu m = new Menu();
                    m.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Usuario y/o contraseña inválidos");
                }
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

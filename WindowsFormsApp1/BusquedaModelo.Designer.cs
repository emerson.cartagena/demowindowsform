﻿namespace WindowsFormsApp1
{
    partial class BusquedaModelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.marcaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSAutos = new WindowsFormsApp1.DSAutos();
            this.marcaTableAdapter = new WindowsFormsApp1.DSAutosTableAdapters.marcaTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idmodelosDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modeloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.asientosDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.combustibleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearmodeloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idmarcaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.modelosTableAdapter = new WindowsFormsApp1.DSAutosTableAdapters.modelosTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.marcaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSAutos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.marcaBindingSource;
            this.comboBox1.DisplayMember = "marca";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(84, 60);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(310, 28);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.ValueMember = "id_marca";
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // marcaBindingSource
            // 
            this.marcaBindingSource.DataMember = "marca";
            this.marcaBindingSource.DataSource = this.dSAutos;
            // 
            // dSAutos
            // 
            this.dSAutos.DataSetName = "DSAutos";
            this.dSAutos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // marcaTableAdapter
            // 
            this.marcaTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idmodelosDataGridViewTextBoxColumn,
            this.modeloDataGridViewTextBoxColumn,
            this.asientosDataGridViewTextBoxColumn,
            this.combustibleDataGridViewTextBoxColumn,
            this.precioDataGridViewTextBoxColumn,
            this.yearmodeloDataGridViewTextBoxColumn,
            this.idmarcaDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.modelosBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 114);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(912, 390);
            this.dataGridView1.TabIndex = 1;
            // 
            // idmodelosDataGridViewTextBoxColumn
            // 
            this.idmodelosDataGridViewTextBoxColumn.DataPropertyName = "id_modelos";
            this.idmodelosDataGridViewTextBoxColumn.HeaderText = "id_modelos";
            this.idmodelosDataGridViewTextBoxColumn.Name = "idmodelosDataGridViewTextBoxColumn";
            this.idmodelosDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modeloDataGridViewTextBoxColumn
            // 
            this.modeloDataGridViewTextBoxColumn.DataPropertyName = "modelo";
            this.modeloDataGridViewTextBoxColumn.HeaderText = "modelo";
            this.modeloDataGridViewTextBoxColumn.Name = "modeloDataGridViewTextBoxColumn";
            this.modeloDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // asientosDataGridViewTextBoxColumn
            // 
            this.asientosDataGridViewTextBoxColumn.DataPropertyName = "asientos";
            this.asientosDataGridViewTextBoxColumn.HeaderText = "asientos";
            this.asientosDataGridViewTextBoxColumn.Name = "asientosDataGridViewTextBoxColumn";
            this.asientosDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // combustibleDataGridViewTextBoxColumn
            // 
            this.combustibleDataGridViewTextBoxColumn.DataPropertyName = "combustible";
            this.combustibleDataGridViewTextBoxColumn.HeaderText = "combustible";
            this.combustibleDataGridViewTextBoxColumn.Name = "combustibleDataGridViewTextBoxColumn";
            this.combustibleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // precioDataGridViewTextBoxColumn
            // 
            this.precioDataGridViewTextBoxColumn.DataPropertyName = "precio";
            this.precioDataGridViewTextBoxColumn.HeaderText = "precio";
            this.precioDataGridViewTextBoxColumn.Name = "precioDataGridViewTextBoxColumn";
            this.precioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // yearmodeloDataGridViewTextBoxColumn
            // 
            this.yearmodeloDataGridViewTextBoxColumn.DataPropertyName = "year_modelo";
            this.yearmodeloDataGridViewTextBoxColumn.HeaderText = "year_modelo";
            this.yearmodeloDataGridViewTextBoxColumn.Name = "yearmodeloDataGridViewTextBoxColumn";
            this.yearmodeloDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idmarcaDataGridViewTextBoxColumn
            // 
            this.idmarcaDataGridViewTextBoxColumn.DataPropertyName = "id_marca";
            this.idmarcaDataGridViewTextBoxColumn.HeaderText = "id_marca";
            this.idmarcaDataGridViewTextBoxColumn.Name = "idmarcaDataGridViewTextBoxColumn";
            this.idmarcaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modelosBindingSource
            // 
            this.modelosBindingSource.DataMember = "modelos";
            this.modelosBindingSource.DataSource = this.dSAutos;
            // 
            // modelosTableAdapter
            // 
            this.modelosTableAdapter.ClearBeforeFill = true;
            // 
            // BusquedaModelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 528);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBox1);
            this.Name = "BusquedaModelo";
            this.Text = "BusquedaModelo";
            this.Load += new System.EventHandler(this.BusquedaModelo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.marcaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSAutos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private DSAutos dSAutos;
        private System.Windows.Forms.BindingSource marcaBindingSource;
        private DSAutosTableAdapters.marcaTableAdapter marcaTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource modelosBindingSource;
        private DSAutosTableAdapters.modelosTableAdapter modelosTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idmodelosDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modeloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn asientosDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn combustibleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearmodeloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idmarcaDataGridViewTextBoxColumn;
    }
}
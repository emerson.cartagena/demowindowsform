USE [master]
GO
/****** Object:  Database [autos]    Script Date: 28/3/2019 08:09:30 ******/
CREATE DATABASE [autos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'autos', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\autos.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'autos_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\autos_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [autos] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [autos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [autos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [autos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [autos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [autos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [autos] SET ARITHABORT OFF 
GO
ALTER DATABASE [autos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [autos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [autos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [autos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [autos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [autos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [autos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [autos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [autos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [autos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [autos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [autos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [autos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [autos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [autos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [autos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [autos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [autos] SET RECOVERY FULL 
GO
ALTER DATABASE [autos] SET  MULTI_USER 
GO
ALTER DATABASE [autos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [autos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [autos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [autos] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [autos] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'autos', N'ON'
GO
ALTER DATABASE [autos] SET QUERY_STORE = OFF
GO
USE [autos]
GO
/****** Object:  Table [dbo].[marca]    Script Date: 28/3/2019 08:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marca](
	[id_marca] [int] IDENTITY(1,1) NOT NULL,
	[marca] [varchar](15) NULL,
	[pais] [varchar](15) NULL,
 CONSTRAINT [pk_marca] PRIMARY KEY CLUSTERED 
(
	[id_marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modelos]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modelos](
	[id_modelos] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [varchar](15) NULL,
	[asientos] [int] NULL,
	[combustible] [varchar](10) NULL,
	[precio] [smallmoney] NULL,
	[year_modelo] [varchar](4) NULL,
	[id_marca] [int] NULL,
 CONSTRAINT [pk_modelos] PRIMARY KEY CLUSTERED 
(
	[id_modelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[repuestos]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[repuestos](
	[id_rep] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](40) NULL,
	[precio] [smallmoney] NULL,
	[descuento] [int] NULL,
	[id_modelos] [int] NULL,
 CONSTRAINT [pk_repuesto] PRIMARY KEY CLUSTERED 
(
	[id_rep] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ususario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ususario](
	[usuario] [varchar](50) NOT NULL,
	[constraseña] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ususario] PRIMARY KEY CLUSTERED 
(
	[usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[modelos]  WITH CHECK ADD  CONSTRAINT [fk_modelos] FOREIGN KEY([id_marca])
REFERENCES [dbo].[marca] ([id_marca])
GO
ALTER TABLE [dbo].[modelos] CHECK CONSTRAINT [fk_modelos]
GO
ALTER TABLE [dbo].[repuestos]  WITH CHECK ADD  CONSTRAINT [fk_repuestos] FOREIGN KEY([id_modelos])
REFERENCES [dbo].[modelos] ([id_modelos])
GO
ALTER TABLE [dbo].[repuestos] CHECK CONSTRAINT [fk_repuestos]
GO
/****** Object:  StoredProcedure [dbo].[deleteMarca]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteMarca]
(
	@Original_id_marca int,
	@IsNull_marca Int,
	@Original_marca varchar(15),
	@IsNull_pais Int,
	@Original_pais varchar(15)
)
AS
	SET NOCOUNT OFF;
DELETE FROM [marca] WHERE (([id_marca] = @Original_id_marca) AND ((@IsNull_marca = 1 AND [marca] IS NULL) OR ([marca] = @Original_marca)) AND ((@IsNull_pais = 1 AND [pais] IS NULL) OR ([pais] = @Original_pais)))
GO
/****** Object:  StoredProcedure [dbo].[deleteModelo]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteModelo]
(
	@Original_id_modelos int,
	@IsNull_modelo Int,
	@Original_modelo varchar(15),
	@IsNull_asientos Int,
	@Original_asientos int,
	@IsNull_combustible Int,
	@Original_combustible varchar(10),
	@IsNull_precio Int,
	@Original_precio smallmoney,
	@IsNull_year_modelo Int,
	@Original_year_modelo varchar(4),
	@IsNull_id_marca Int,
	@Original_id_marca int
)
AS
	SET NOCOUNT OFF;
DELETE FROM [modelos] WHERE (([id_modelos] = @Original_id_modelos) AND ((@IsNull_modelo = 1 AND [modelo] IS NULL) OR ([modelo] = @Original_modelo)) AND ((@IsNull_asientos = 1 AND [asientos] IS NULL) OR ([asientos] = @Original_asientos)) AND ((@IsNull_combustible = 1 AND [combustible] IS NULL) OR ([combustible] = @Original_combustible)) AND ((@IsNull_precio = 1 AND [precio] IS NULL) OR ([precio] = @Original_precio)) AND ((@IsNull_year_modelo = 1 AND [year_modelo] IS NULL) OR ([year_modelo] = @Original_year_modelo)) AND ((@IsNull_id_marca = 1 AND [id_marca] IS NULL) OR ([id_marca] = @Original_id_marca)))
GO
/****** Object:  StoredProcedure [dbo].[deleteUsuario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteUsuario]
(
	@Original_usuario varchar(50)
)
AS
	SET NOCOUNT OFF;
DELETE FROM [ususario] WHERE (([usuario] = @Original_usuario))
GO
/****** Object:  StoredProcedure [dbo].[insertMarca]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertMarca]
(
	@marca varchar(15),
	@pais varchar(15)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [marca] ([marca], [pais]) VALUES (@marca, @pais);
	
SELECT id_marca, marca, pais FROM marca WHERE (id_marca = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[insertModelo]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertModelo]
(
	@modelo varchar(15),
	@asientos int,
	@combustible varchar(10),
	@precio smallmoney,
	@year_modelo varchar(4),
	@id_marca int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [modelos] ([modelo], [asientos], [combustible], [precio], [year_modelo], [id_marca]) VALUES (@modelo, @asientos, @combustible, @precio, @year_modelo, @id_marca);
	
SELECT id_modelos, modelo, asientos, combustible, precio, year_modelo, id_marca FROM modelos WHERE (id_modelos = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[insertUsuario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertUsuario]
(
	@usuario varchar(50),
	@constraseña varchar(100)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [ususario] ([usuario], [constraseña]) VALUES (@usuario, @constraseña);
	
SELECT usuario, constraseña FROM ususario WHERE (usuario = @usuario)
GO
/****** Object:  StoredProcedure [dbo].[insertUusario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insertUusario]
(
	@usuario varchar(50),
	@constraseña varchar(100)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [ususario] ([usuario], [constraseña]) VALUES (@usuario, @constraseña);
	
SELECT usuario, constraseña FROM ususario WHERE (usuario = @usuario)
GO
/****** Object:  StoredProcedure [dbo].[selectMarca]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectMarca]
AS
	SET NOCOUNT ON;
SELECT marca.*
FROM   marca
GO
/****** Object:  StoredProcedure [dbo].[selectModelo]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectModelo]
AS
	SET NOCOUNT ON;
SELECT modelos.*
FROM   modelos
GO
/****** Object:  StoredProcedure [dbo].[selectUsuario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectUsuario]
AS
	SET NOCOUNT ON;
SELECT ususario.*
FROM   ususario
GO
/****** Object:  StoredProcedure [dbo].[updateMarca]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[updateMarca]
(
	@marca varchar(15),
	@pais varchar(15),
	@Original_id_marca int,
	@IsNull_marca Int,
	@Original_marca varchar(15),
	@IsNull_pais Int,
	@Original_pais varchar(15),
	@id_marca int
)
AS
	SET NOCOUNT OFF;
UPDATE [marca] SET [marca] = @marca, [pais] = @pais WHERE (([id_marca] = @Original_id_marca) AND ((@IsNull_marca = 1 AND [marca] IS NULL) OR ([marca] = @Original_marca)) AND ((@IsNull_pais = 1 AND [pais] IS NULL) OR ([pais] = @Original_pais)));
	
SELECT id_marca, marca, pais FROM marca WHERE (id_marca = @id_marca)
GO
/****** Object:  StoredProcedure [dbo].[updateModelo]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[updateModelo]
(
	@modelo varchar(15),
	@asientos int,
	@combustible varchar(10),
	@precio smallmoney,
	@year_modelo varchar(4),
	@id_marca int,
	@Original_id_modelos int,
	@IsNull_modelo Int,
	@Original_modelo varchar(15),
	@IsNull_asientos Int,
	@Original_asientos int,
	@IsNull_combustible Int,
	@Original_combustible varchar(10),
	@IsNull_precio Int,
	@Original_precio smallmoney,
	@IsNull_year_modelo Int,
	@Original_year_modelo varchar(4),
	@IsNull_id_marca Int,
	@Original_id_marca int,
	@id_modelos int
)
AS
	SET NOCOUNT OFF;
UPDATE [modelos] SET [modelo] = @modelo, [asientos] = @asientos, [combustible] = @combustible, [precio] = @precio, [year_modelo] = @year_modelo, [id_marca] = @id_marca WHERE (([id_modelos] = @Original_id_modelos) AND ((@IsNull_modelo = 1 AND [modelo] IS NULL) OR ([modelo] = @Original_modelo)) AND ((@IsNull_asientos = 1 AND [asientos] IS NULL) OR ([asientos] = @Original_asientos)) AND ((@IsNull_combustible = 1 AND [combustible] IS NULL) OR ([combustible] = @Original_combustible)) AND ((@IsNull_precio = 1 AND [precio] IS NULL) OR ([precio] = @Original_precio)) AND ((@IsNull_year_modelo = 1 AND [year_modelo] IS NULL) OR ([year_modelo] = @Original_year_modelo)) AND ((@IsNull_id_marca = 1 AND [id_marca] IS NULL) OR ([id_marca] = @Original_id_marca)));
	
SELECT id_modelos, modelo, asientos, combustible, precio, year_modelo, id_marca FROM modelos WHERE (id_modelos = @id_modelos)
GO
/****** Object:  StoredProcedure [dbo].[updateUsuario]    Script Date: 28/3/2019 08:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[updateUsuario]
(
	@usuario varchar(50),
	@constraseña varchar(100),
	@Original_usuario varchar(50)
)
AS
	SET NOCOUNT OFF;
UPDATE [ususario] SET [usuario] = @usuario, [constraseña] = @constraseña WHERE (([usuario] = @Original_usuario));
	
SELECT usuario, constraseña FROM ususario WHERE (usuario = @usuario)
GO
USE [master]
GO
ALTER DATABASE [autos] SET  READ_WRITE 
GO

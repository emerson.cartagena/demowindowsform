﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class BusquedaModelo : Form
    {

        public bool inicializado = false;

        public BusquedaModelo()
        {
            InitializeComponent();
        }

        private void BusquedaModelo_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dSAutos.modelos' Puede moverla o quitarla según sea necesario.
            this.modelosTableAdapter.Fill(this.dSAutos.modelos);
            // TODO: esta línea de código carga datos en la tabla 'dSAutos.marca' Puede moverla o quitarla según sea necesario.
            this.marcaTableAdapter.Fill(this.dSAutos.marca);
            inicializado = true;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if(inicializado)
            {
                try
                {
                    int idmarca = 0;
                    int.TryParse(comboBox1.SelectedValue.ToString(), out idmarca);
                    this.modelosTableAdapter.FillByMarca(this.dSAutos.modelos, new System.Nullable<int>(((int)(System.Convert.ChangeType(idmarca, typeof(int))))));
                }
                catch (System.Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }           
        }
    }
}

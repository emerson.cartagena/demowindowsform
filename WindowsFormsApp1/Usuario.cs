﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent();
        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dSAutos.ususario' Puede moverla o quitarla según sea necesario.
            this.ususarioTableAdapter.Fill(this.dSAutos.ususario);
         

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            MantoUsuario usr = new MantoUsuario();
            if (usr.ShowDialog() == DialogResult.OK)
            {
                this.ususarioTableAdapter.Fill(this.dSAutos.ususario);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                //obtengo usuario
                string user = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();

                if (e.ColumnIndex == 2)
                {
                    MantoUsuario usr = new MantoUsuario(user);
                    if (usr.ShowDialog() == DialogResult.OK)
                    {
                        this.ususarioTableAdapter.Fill(this.dSAutos.ususario);
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if(MessageBox.Show("¿Esta seguro que desea eliminar el usuario seleccionado?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        DSAutosTableAdapters.ususarioTableAdapter tableAdapter = new DSAutosTableAdapters.ususarioTableAdapter();
                        int error = 0;
                        error = tableAdapter.Delete(user);                    
                        if (error == 1)
                        {
                           this.ususarioTableAdapter.Fill(this.dSAutos.ususario);
                        }
                        else
                        {
                            MessageBox.Show("Ha ocurrido un error al eliminar el usuario");
                        }
                }
                }
            }

            
        }
    }
}
